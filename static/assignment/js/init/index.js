require([
    "jquery",
    "static/assignment/js/models/models",
    "static/assignment/js/views/search"
], function( $, Models, Search ){

    var Project = {
        Models : {
            Entry : Models.Entry,
            EntryList : Models.EntryList
        },
        Views : {
            Search : Search
        },
        Objects : {},
    };

    Project.Objects.rootView = new Search();

});