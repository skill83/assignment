require.config({
    paths: {
        "jquery"        : "static/bower_components/jquery/dist/jquery.min",
        "underscore"    : "static/bower_components/underscore/underscore-min",
        "backbone"      : "static/bower_components/backbone/backbone",
        "jquery.icheck" : "static/bower_components/iCheck/icheck.min"
    }
});
