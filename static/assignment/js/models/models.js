if ( typeof define === "function" && define.amd ) {
    define(
        [
            'backbone'
        ],
        function( Backbone )
    {

        'use strict';

        var Entry = Backbone.Model.extend({

            formatDate : function(date) {
                var d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();

                if (month.length < 2) month = '0' + month;
                if (day.length < 2) day = '0' + day;

                return [year, month, day].join('-');
            },

            initialize : function(){
                var me   = this,
                    date = me.formatDate( me.get("createdAt") );

                me.set( "datetime" , date );
            }

        });

        var EntryList = Backbone.Collection.extend({
            model: Entry
        });

        return {
            Entry     : Entry,
            EntryList : EntryList
        };

    });

} else {
    throw "RequireJs is mandatory";
}

