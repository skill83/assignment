if ( typeof define === "function" && define.amd ) {
    define(
        [
            "underscore",
            "jquery",
            "backbone",
            "static/assignment/js/views/base_views",
            "static/assignment/js/views/prezi_item"
        ],
        function( _, $, Backbone, BaseViews, PreziItem )
    {
        "use strict";

        var V = BaseViews.BaseListView.extend({

            baseChildrenView           : PreziItem,
            baseChildrenViewTemplateId : null,

            bindEvents : function() {
                var me = this;

                me.listenTo(me.collection, 'sync', me.render);
            }

        });


        return V;

    });

} else {
    throw "RequireJs is mandatory";
}
