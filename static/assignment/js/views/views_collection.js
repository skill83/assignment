if ( typeof define === "function" && define.amd ) {
    define([
        "underscore",
        "backbone"
    ], function( _, Backbone ) {

        "use strict";

        var Collection = Backbone.ViewsCollection = function(views, options) {
            var me = this;

            options || ( options = {} );

            if( options.view ){
                me.view = options.view;
            }

            if( options.comparator !== void(0) ){
                me.comparator = options.comparator;
            }

            me.model = me.view;
            me.models = me.views = [];
            me._reset();
            me.initialize.apply( me, arguments );
            if( views ){
                me.reset( views, _.extend({
                    silent : true
                }, options ) );
            }
        };

        _.extend(Collection.prototype, Backbone.Collection.prototype, {

            view : Backbone.View,

            sync : function() {
                // View not need to sync...
                return this;
            },

            // Create a new instance of a model in this collection. Add the model to the
            // collection immediately, unless `wait: true` is passed, in which case we
            // wait for the server to agree.
            create : function( model, options ){
                var me = this;

                options = options ? _.clone( options ) : {};

                if( !model ){
                    model = new me.view();
                }

                if ( !( model = me._prepareModel( model, options ) ) ) return false;

                me.add( model, options );

                return model;
            },

            _prepareModel : function( attrs, options ){

                if ( attrs instanceof Backbone.View) {
                    if ( !attrs.viewCollection ){
                        attrs.viewCollection = this;
                    }
                    return attrs;
                }

                // Only Backbone.View allowed!
                return false;
            },

            // Internal method to remove a model's ties to a collection.
            _removeReference: function( model ){
                var me = this;

                if ( me === model.viewCollection ){
                    delete model.viewCollection;
                }

                model.off( "all", me._onModelEvent, me );
            },

            _reset: function() {
                var me = this;

                me.length = 0;
                me.views = me.models = [];
                me._byId  = {};
            }
        });

        var unusedMethods = ["toJSON", "fetch"];

        // Unused method
        _.each( unusedMethods, function( meth ){
            delete( Collection.prototype.meth );
        });

        // Copy extend form Backbone.Collection
        Collection.extend = Backbone.Collection.extend;

        return {
            ViewsCollection:Collection
        };

    });
} else {
    throw "RequireJs is mandatory";
}