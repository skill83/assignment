if ( typeof define === "function" && define.amd ) {
    define(
        [
            "underscore",
            "jquery",
            "backbone",
            "static/assignment/js/views/base_views"
        ],
        function( _, $, Backbone, BaseViews )
    {
        "use strict";

        var V = BaseViews.BaseDetailView.extend({

            templateId : "#prezi_item_tpl",
            tagName    : "div",

            attributes : function () {
                var me = this;

                return {
                    "class" : "col-xs-12 col-sm-4 col-md-4 col-lg-3"
                };
            }


        });


        return V;

    });

} else {
    throw "RequireJs is mandatory";
}
