if ( typeof define === "function" && define.amd ) {
    define(
        [
            "underscore",
            "jquery",
            "backbone",
            "static/assignment/js/views/base_views",
            "static/assignment/js/models/models",
            "static/assignment/js/views/list_view",
            "jquery.icheck"

        ],
        function( _, $, Backbone, BaseViews, Models, ListView)
    {
        "use strict";

        var V = BaseViews.BaseView.extend({

            el : "body",

            events : {
                "submit .js-searchForm" : "updateResults",
                "ifChanged .js-order"   : "updateResults"
            },

            preInitialize : function(){
                var me = this;

                me.initListView();
                me.initCustomCheckbox();
            },

            initCustomCheckbox : function(){
                var me = this;

                $(".js-customChk").iCheck({
                    checkboxClass: 'icheckbox_square-blue'
                });
            },

            initListView : function(){
                var me          = this,
                    $listViewEl = $( ".js-listView" ),
                    fetchUrl    = $( ".js-searchForm" ).data( "api" );

                if( !me.resultsCollection ){
                    me.resultsCollection = new Models.EntryList();
                }

                if( !me.resultsListView ){
                    me.resultsListView = new ListView({
                        el         : $listViewEl,
                        collection : me.resultsCollection
                    });
                }

                me.resultsCollection.fetch({
                    url : fetchUrl
                });
            },

            getForm : function($el){
                var me = this;

                return $el.hasClass( ".js-searchForm" ) ? $el : $el.closest( ".js-searchForm" );
            },

            updateResults : function(e){
                var me = this,
                    $form       = me.getForm( $( e.currentTarget ) ),
                    fetchUrl    = $form.data( "api" ),
                    title       = $( ".js-title", $form ).val(),
                    orderByDate = $( ".js-order", $form ).prop("checked");

                e&&e.preventDefault();

                me.resultsCollection.fetch({
                    url : fetchUrl+"?"+$form.serialize()
                });

            }

        });


        return V;

    });

} else {
    throw "RequireJs is mandatory";
}
