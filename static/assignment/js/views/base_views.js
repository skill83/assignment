if( typeof define === "function" && define.amd ) {
    define(
        [
            "underscore",
            "jquery",
            "backbone",
            "static/assignment/js/views/views_collection"
        ],
        function( _, $, Backbone, ViewsCollection )
    {
        "use strict";

        var BaseView = Backbone.View.extend({

            templateId : null,
            elCss : null,

            _configure : function( options ){
                var me = this;

                if( me.options ){
                    options = _.extend( {}, _.result( me, "options" ), options );
                }

                _.extend( me, _.pick( options, viewOptions ) );

                me.options = options;
            },

            _super : function( base, method, args ){
                var me = this;

                return base.prototype[method].apply(me, args || []);
            },

            initialize : function( options ){
                var me = this;
                me.preInitialize( options );

                me._super( Backbone.View, "initialize", arguments );
                me.bindEvents( options );

                me.postInitialize( options );
            },

            postInitialize : function( options ){
                // Implement in concrete class
            },

            preInitialize : function( options ){
                // Implement in concrete class
            },

            bindEvents : function( options ) {
                // Implement in concrete class
            },

            getTemplate : function(){
                var me = this;

                if ( me.template ) {
                    if ( _.isFunction( me.template ) ){
                        return me.template.apply( me );
                    }
                    return me.template;
                }
                if ( me.templateId ) {
                    var templateId = me.templateId;
                    if ( _.isFunction( me.templateId ) ){
                        templateId = me.templateId.apply( me );
                    }
                    return _.template( $( templateId ).html() );
                }

                return null;
            },

            getContext : function(){
                return {};
            },

            preRender : function(){
                var me = this;
                return me;
            },

            postRender : function(){
                var me = this;
                return me;
            },

            render : function(){
                var me = this,
                    tmpl = me.getTemplate(),
                    context = me.getContext();

                me.preRender();

                if( tmpl ){
                    me.$el.html( tmpl( context ) );
                }

                me.postRender();

                return me;
            },


            detach : function() {
                var me = this;

                me.$el.detach();

                return this;
            }
        });

        var BaseDetailView = BaseView.extend({

            bindEvents : function( options ){
                var me = this;
                me.listenTo(me.model, "all", me.handlerAll);
            },

            handlerAll : function(){
                var me = this;
                me.render();
            },

            getContext : function(){
                var me = this;
                return {
                    model : me.model && me.model.toJSON()
                };
            }

        });

        var BaseListView = BaseView.extend({

            baseChildrenView       : BaseDetailView,
            baseChildrenCollection : null,
            baseChildrenWrapper    : null,

            bindEvents : function( options ){
                var me = this;
                me.listenTo( me.collection, "all", me.handlerAll );
            },

            handlerAll : function(){
                var me = this;
                me.render();
            },

            preRender : function(){
                var me = this;
                me.createChildrenViews();
                return me;
            },

            postRender : function(){
                var me = this;
                return me.renderChildrenViews();
            },

            createChildrenViews : function(){
                var me = this,
                    views = [];

                var Cc = me.baseChildrenCollection || Backbone.ViewsCollection;

                if ( !me.childrenCollection ) {
                    me.childrenCollection = new Cc();
                }

                me.collection.each(function( obj ){
                    views.push( me.createChildView( obj ) );
                });

                me.childrenCollection.reset( views );

                return me;
            },

            createChildView : function( obj ){
                var me = this;

                return new me.baseChildrenView({
                    model : obj
                });
            },

            renderChildrenViews : function(){
                var me = this;
                var $wrapper;

                if( me.baseChildrenWrapper ){
                    $wrapper =me.$( me.baseChildrenWrapper );
                }else{
                    $wrapper = me.$el;
                }

                $wrapper.empty();

                if( me.childrenCollection.length===0 ){
                    $wrapper.append( "<p>No matches found</p>" );
                }else{
                    me.childrenCollection.each(function( view ) {
                        $wrapper.append( view.render().$el );
                    });
                }

                return me;
            },

            emptyCollection : function(){
                return "";
            },

            removeChildrenViews : function(){
                var me = this;

                if ( me.childrenCollection ){
                    me.childrenCollection.each(function( view ){
                        view.remove();
                    });
                    me.childrenCollection.reset();
                }

                return me;
            },

            unbindCustomEvents : function() {
                return this;
            },

            remove : function() {
                var me = this;

                me.unbindCustomEvents();
                me.removeChildrenViews();

                return me._super( BaseView, "remove", arguments );
            },

            getContext : function() {
                var me = this;
                return {
                    collection : me.collection && me.collection.toJSON()
                };
            }
        });


        return {
            BaseView       : BaseView,
            BaseDetailView : BaseDetailView,
            BaseListView   : BaseListView
        };

    });

} else {
    throw "RequireJs is mandatory";
}
