module.exports = function( grunt ) {

    grunt.initConfig({
        cssResources: [],
        pkg: grunt.file.readJSON( "package.json" ),
        copy: {
            html: {
                flatten : true,
                expand  : true,
                src     : "index.html",
                dest    : "dist/"
            },
            fonts:{
                flatten : true,
                expand  : true,
                src     : "static/bower_components/font-awesome/fonts/*",
                dest    : "dist/fonts"
            },
            imgs:{
                flatten : true,
                expand  : true,
                src     : "static/bower_components/iCheck/skins/square/blue.png",
                dest    : "dist/"
            },
            imgsx2:{
                flatten : true,
                expand  : true,
                src     : "static/bower_components/iCheck/skins/square/blue@2x.png",
                dest    : "dist/"
            }
        },
        less: {
            options: {
                cleancss: true,
                compress: true,
                plugins: [
                    new ( require( "less-plugin-clean-css" ) )( { keepSpecialComments: 0 } )
                ]
            },
            compile: {
                expand: true,
                cwd: "static/assignment/less/",
                src: [
                    "*.less",
                    "!project_variables.less"
                ],
                ext: ".css",
                dest: "static/assignment/css/"
            }
        },
        autoprefixer: {
            options: {
                browsers: [ "last 5 versions" ]
            },
            compile: {
                expand: true,
                cwd: "static/assignment/css/",
                src: [
                    "*.css"
                ],
                ext: ".css",
                dest: "static/assignment/css/"
            }
        },
        watch: {
            default: {
                files: "static/assignment/less/*.less",
                tasks: [ "compileCss" ],
            }
        },
        htmlrefs: {
            dist: {
                src: "./index.html",
                dest: "./dist/index.html"
            }
        },
        requirejs: {
            dist: {
                options: {
                    baseUrl: "./",
                    mainConfigFile: "./static/assignment/js/conf/main.js",
                    name: "./static/assignment/js/init/index.js",
                    out: "./dist/index.js",
                    include: [
                      "./static/bower_components/requirejs/require"
                    ]
                }
            }
        },
        replace: {
            dist: {
                files: [
                    {
                        cwd: "./dist",
                        dest: "./dist",
                        expand: true,
                        src: [ "index.html" ]
                    }
                ],
                options: {
                    patterns: [
                        {
                            match: /\<\!\-\-build\-css\-start[\s\S]*build\-css\-end\-\-\>/,
                            replacement: function ( matchedString ) {
                                var cssArray = matchedString.match( /(href\s?\=\s?[\'\"])([^\'\"]*)([\'\"])/g );
                                cssArray.forEach( function( element ) {
                                    var resourceTarget = element.match( /(href\s?\=\s?[\'\"])([^\'\"]*)([\'\"])/ )[ 2 ];
                                    var targetConfig = grunt.config( 'cssResources' );
                                    targetConfig.push( "./" + resourceTarget );
                                    grunt.config( "cssResources", targetConfig );
                                });

                                return '<link rel="stylesheet" media="screen" href="style.css"/>';
                            }
                        }
                    ]
                }
            }
        },

        concat: {
            css: {
                src: [ "<%= cssResources %>" ],
                dest: "./dist/style.css"
            }
        }

    });

    grunt.loadNpmTasks( "grunt-contrib-copy" );
    grunt.loadNpmTasks( "grunt-contrib-less" );
    grunt.loadNpmTasks( "grunt-autoprefixer" );
    grunt.loadNpmTasks( "grunt-contrib-watch" );
    grunt.loadNpmTasks( "grunt-htmlrefs" );
    grunt.loadNpmTasks( "grunt-contrib-requirejs" );
    grunt.loadNpmTasks( "grunt-replace" );
    grunt.loadNpmTasks( "grunt-contrib-concat" );

    grunt.registerTask( "dist", [ "compileCss", "copy", "htmlrefs:dist", "requirejs:dist", "replace:dist", "concat:css" ]);
    grunt.registerTask( "compileCss", [ "less", "autoprefixer" ] );
    grunt.registerTask( "default","watch" );

};
