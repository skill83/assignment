var request = require("supertest"),
    fs      = require("fs");

describe("Search API", function() {

    var url = "http://localhost:8081";

    it("should answer with a json to a xhr request", function(done) {
        request(url)
            .get('/search')
            .set('X-Requested-With', 'xmlhttprequest')
            .expect(200)
            .expect('Content-Type', /json/, done);
    });

    describe("Search", function() {

        it("should return all results without params", function(done) {
            var allLength;
            fs.readFile( __dirname + "/../" + "prezis.json", "utf8", function( err, data ) {
                if ( err ){
                    throw err;
                }
                allLength = JSON.parse( data ).length;
                request(url)
                    .get('/search')
                    .set('X-Requested-With', 'xmlhttprequest')
                    .end(function(err,res) {
                        if (res.body.length !== allLength) {
                            throw err;
                        }

                        done();
                    });
            });
        });

        it("should accept a title parameter", function(done) {
            request(url)
                .get('/search?title=foo')
                .set('X-Requested-With', 'xmlhttprequest')
                .expect(200,done);
        });

        it("should accept an order_by parameter", function(done) {
            request(url)
                .get('/search?order_by=foo')
                .set('X-Requested-With', 'xmlhttprequest')
                .expect(200)
                .end(function(err,res) {
                    if ( err ){
                        throw err;
                    }
                    done();
                });
        });

        it("should accept an order_in parameter", function(done) {
            request(url)
                .get('/search?order_in=foo')
                .set('X-Requested-With', 'xmlhttprequest')
                .expect(200)
                .end(function(err,res) {
                    if ( err ){
                        throw err;
                    }
                    done();
                });
        });


        describe("should search by title", function() {

            it("search for foo", function(done) {
                request(url)
                    .get('/search?title=foo')
                    .set('X-Requested-With', 'xmlhttprequest')
                    .end(function(err,res) {
                        if ( err ){
                            throw err;
                        }
                        if (res.body.length !== 0) {
                            throw "Mismatch results length";
                        }
                        done();
                    });
                });

            it("search for duis", function(done) {
                request(url)
                    .get('/search?title=duis')
                    .set('X-Requested-With', 'xmlhttprequest')
                    .end(function(err,res) {
                        if ( err ){
                            throw err;
                        }
                        if (res.body.length !== 11) {
                            throw "Mismatch results length";
                        }
                        done();
                    });
                });

            it("search for consectetur", function(done) {
                request(url)
                    .get('/search?title=consectetur')
                    .set('X-Requested-With', 'xmlhttprequest')
                    .end(function(err,res) {
                        if ( err ){
                            throw err;
                        }
                        if (res.body.length !== 6) {
                            throw "Mismatch results length";
                        }
                        done();
                    });
                });
        });

        describe("should orderd by creation date", function() {

            it("front ordering", function(done) {
                var resultsLength;

                request(url)
                    .get('/search?order_by=date')
                    .set('X-Requested-With', 'xmlhttprequest')
                    .end(function(err,res) {
                        resultsLength = res.body.length;

                        if(resultsLength<2){
                            throw 'cannot test ordering by date; not enough results';
                        }else{
                            if( Date(res.body[0].createdAt) > Date(res.body[1].createdAt) ){
                                throw 'ordering not working';
                            }
                            if(res.body[2]){
                                if( Date(res.body[1].createdAt) > Date(res.body[2].createdAt) ){
                                    throw 'ordering not working';
                                }
                            }
                            if(res.body[3]){
                                if( Date(res.body[2].createdAt) > Date(res.body[3].createdAt) ){
                                    throw 'ordering not working';
                                }
                            }
                        }

                        done();
                    });
            });

            it("reverse ordering", function(done) {
                var resultsLength;

                request(url)
                    .get('/search?order_by=date&order_in=asc')
                    .set('X-Requested-With', 'xmlhttprequest')
                    .end(function(err,res) {
                        resultsLength = res.body.length;

                        if(resultsLength<2){
                            throw 'cannot test ordering by date; not enough results';
                        }else{
                            if( Date(res.body[0].createdAt) < Date(res.body[1].createdAt) ){
                                throw 'ordering not working';
                            }
                            if(res.body[2]){
                                if( Date(res.body[1].createdAt) < Date(res.body[2].createdAt) ){
                                    throw 'ordering not working';
                                }
                            }
                            if(res.body[3]){
                                if( Date(res.body[2].createdAt) < Date(res.body[3].createdAt) ){
                                    throw 'ordering not working';
                                }
                            }
                        }

                        done();
                    });
            });

        });

  });


});