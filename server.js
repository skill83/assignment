var express = require("express"),
    fs      = require("fs"),
    url     = require("url"),
    _       = require("underscore"),
    app     = express();

/**
 * Filter data
 * @param {String} title, item title
 * @param {Object} data, items data
 * @return {Object} returns items created by the name
 */
var filterByTitle = function( name, data ){

    var titleFilter = new RegExp( name, "i" );

    return _.filter( data, function( obj ){
        return titleFilter.test( obj.title );
    });

};

/**
 * Order data
 * @param {Object} data, items data
 * @return {Object} returns items ordered by date desc
 */
var orderByDate = function( data ){

    return data.sort( function( a, b ){
        return new Date( b.createdAt ) - new Date( a.createdAt );
    });

};

/**
 * Send parsed results
 * Open the data file and returns json results
 * if a valid qs param is given returns the correct subset of data
 */
var sendResults = function( req, res ) {

    var url_parts = url.parse( req.url, true ),
        query     = url_parts.query,
        jsonData;

    fs.readFile( __dirname + "/" + "prezis.json", "utf8", function( err, data ) {

        if ( err ){
            throw err;
        }

        var jsonData = JSON.parse( data );

        if( query.title ){
            jsonData = filterByTitle( query.title, jsonData );
        }

        if( query.order_by === "date" ){
            jsonData = orderByDate( jsonData );
        }

        if( query.order_in === "asc" ){
            jsonData.reverse();
        }

        res.type('json').json( jsonData );

    });

};

/**
 * Allow CORS
 * Enabling cors for retrieve data from another port or static site
 */
app.use( function( req, res, next ){

    res.header( "Access-Control-Allow-Origin", "*" );
    res.header( "Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept" );
    next();

});

/**
 * HTTP GET /search
 * Returns: the list of tasks in JSON format
 */
app.get( "/search", [ sendResults ]);

var server = app.listen( 8081, function(){

    var host = server.address().address,
        port = server.address().port;

    console.log( "App listening at http://%s:%s", host, port );

});